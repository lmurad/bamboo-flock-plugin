package com.atlassian.bamboo.flock;

import com.atlassian.bamboo.deployments.results.DeploymentResult;
import com.atlassian.bamboo.notification.NotificationRecipient;
import com.atlassian.bamboo.notification.NotificationTransport;
import com.atlassian.bamboo.notification.recipients.AbstractNotificationRecipient;
import com.atlassian.bamboo.deployments.notification.DeploymentResultAwareNotificationRecipient;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.plugin.descriptor.NotificationRecipientModuleDescriptor;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.template.TemplateRenderer;
import com.atlassian.bamboo.variable.CustomVariableContext;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;

public class FlockNotificationRecipient extends AbstractNotificationRecipient implements DeploymentResultAwareNotificationRecipient,
                                                                                           NotificationRecipient.RequiresPlan,
                                                                                           NotificationRecipient.RequiresResultSummary

{
    private static String CHANNEL = "channel";
    private static String TOKEN = "token";
    private static String APPID = "appid";
    private static String ICON_URL = "iconUrl";
    private static String BOT_NAME = "botName";

    private String channel = null;
    private String iconUrl = null;
    private String token = null;
    private String appid = null;
    private String botName = null;

    private TemplateRenderer templateRenderer;

    private ImmutablePlan plan;
    private ResultsSummary resultsSummary;
    private DeploymentResult deploymentResult;
    private CustomVariableContext customVariableContext;

    @Override
    public void populate(@NotNull Map<String, String[]> params)
    {
        for (String next : params.keySet())
        {
            System.out.println("next = " + next);
        }
        if (params.containsKey(CHANNEL))
        {
            int i = params.get(CHANNEL).length - 1;
            this.channel = params.get(CHANNEL)[i];
        }
        if (params.containsKey(ICON_URL)) {
            int i = params.get(ICON_URL).length - 1;
            this.iconUrl = params.get(ICON_URL)[i];
        }
        if (params.containsKey(BOT_NAME)) {
            int i = params.get(BOT_NAME).length - 1;
            this.botName = params.get(BOT_NAME)[i];
        }
        if (params.containsKey(TOKEN)) {
            int i = params.get(TOKEN).length - 1;
            this.token = params.get(TOKEN)[i];
        }
        if (params.containsKey(APPID)) {
            int i = params.get(APPID).length - 1;
            this.appid = params.get(APPID)[i];
        }
    }

    @Override
    public void init(@Nullable String configurationData)
    {

        if (StringUtils.isNotBlank(configurationData))
        {
            String delimiter = "\\|";

            String[] configValues = configurationData.split(delimiter);

            if (configValues.length > 0) {
                channel = configValues[0];
            }
            if (configValues.length > 1) {
                iconUrl = configValues[1];
            }
            if (configValues.length > 2) {
                botName = configValues[2];
            }
            if (configValues.length > 3) {
                token = configValues[3];
            }
            if (configValues.length > 4) {
                appid = configValues[4];
            }
        }
    }

    @NotNull
    @Override
    public String getRecipientConfig()
    {
        String delimiter = "|";

        StringBuilder recipientConfig = new StringBuilder();
        if (StringUtils.isNotBlank(channel)) {
            recipientConfig.append(channel);
        }
        recipientConfig.append(delimiter);
        if (StringUtils.isNotBlank(iconUrl)) {
            recipientConfig.append(iconUrl);
        }
        recipientConfig.append(delimiter);
        if (StringUtils.isNotBlank(botName)) {
            recipientConfig.append(botName);
        }
        recipientConfig.append(delimiter);
        if (StringUtils.isNotBlank(token)) {
            recipientConfig.append(token);
        }
        recipientConfig.append(delimiter);
        if (StringUtils.isNotBlank(appid)) {
            recipientConfig.append(appid);
        }
        return recipientConfig.toString();
    }

    @NotNull
    @Override
    public String getEditHtml()
    {
        String editTemplateLocation = ((NotificationRecipientModuleDescriptor)getModuleDescriptor()).getEditTemplate();
        return templateRenderer.render(editTemplateLocation, populateContext());
    }

    private Map<String, Object> populateContext()
    {
        Map<String, Object> context = Maps.newHashMap();

        if (channel != null)
        {
            context.put(CHANNEL, channel);
        }

        if (iconUrl != null) {
            context.put(ICON_URL, iconUrl);
        }

        if (botName != null) {
            context.put(BOT_NAME, botName);
        }

        if (token != null) {
            context.put(TOKEN, token);
        }

        if (appid != null) {
            context.put(APPID, appid);
        }

        System.out.println("populateContext = " + context.toString());

        return context;
    }

    @NotNull
    @Override
    public String getViewHtml()
    {
        String editTemplateLocation = ((NotificationRecipientModuleDescriptor)getModuleDescriptor()).getViewTemplate();
        return templateRenderer.render(editTemplateLocation, populateContext());
    }



    @NotNull
    public List<NotificationTransport> getTransports()
    {
        List<NotificationTransport> list = Lists.newArrayList();
        list.add(new FlockNotificationTransport(channel, iconUrl, botName, token, appid, plan, resultsSummary, deploymentResult, customVariableContext));
        return list;
    }

    public void setPlan(@Nullable final Plan plan)
    {
        this.plan = plan;
    }

    public void setPlan(@Nullable final ImmutablePlan plan)
    {
        this.plan = plan;
    }

    public void setDeploymentResult(@Nullable final DeploymentResult deploymentResult)
    {
        this.deploymentResult = deploymentResult;
    }

    public void setResultsSummary(@Nullable final ResultsSummary resultsSummary)
    {
        this.resultsSummary = resultsSummary;
    }

    //-----------------------------------Dependencies
    public void setTemplateRenderer(TemplateRenderer templateRenderer)
    {
        this.templateRenderer = templateRenderer;
    }

    public void setCustomVariableContext(CustomVariableContext customVariableContext) { this.customVariableContext = customVariableContext; }
}
