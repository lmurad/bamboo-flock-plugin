package com.atlassian.bamboo.flock;

import com.atlassian.bamboo.notification.buildhung.BuildHungNotification;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.variable.CustomVariableContext;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.http.HttpHost;
import org.apache.http.HttpRequest;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.HttpContext;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class FlockNotificationTransportTest
{
    private final String WEBHOOK_URL = "https://api.flock.co/v1/chat.sendMessage";
    private final String CHANNEL = "#theRoom";
    private final String ICON_URL = "http://upload.wikimedia.org/wikipedia/commons/thumb/4/40/Wave.svg/170px-Wave.svg.png";
    private final String BOT_NAME = "myBamboo";
    private final String TOKEN = "token";
    private final String APPID = "appid";

    @Mock
    private Project project;
    @Mock
    private Plan plan;
    @Mock
    private CustomVariableContext customVariableContext;

    @Test
    public void testCorrectUrlsAreHit()
    {
        when(project.getKey()).thenReturn("BAM");
        when(plan.getProject()).thenReturn(project);
        when(plan.getBuildKey()).thenReturn("MAIN");
        when(plan.getName()).thenReturn("Main");
        when(customVariableContext.substituteString("alamakotaakotma")).thenReturn("alamakotaakotma");
        when(customVariableContext.substituteString("alamakotaakotmaapitoken")).thenReturn("alamakotaakotmaapitoken");
        when(customVariableContext.substituteString("#theRoom")).thenReturn("#theRoom");
        when(customVariableContext.substituteString(ICON_URL)).thenReturn(ICON_URL);
        when(customVariableContext.substituteString(BOT_NAME)).thenReturn(BOT_NAME);
        when(customVariableContext.substituteString(TOKEN)).thenReturn(TOKEN);
        when(customVariableContext.substituteString(APPID)).thenReturn(APPID);


        final PlanResultKey planResultKey = PlanKeys.getPlanResultKey("BAM-MAIN", 3);

        BuildHungNotification notification = new BuildHungNotification()
        {
            public String getHtmlImContent()
            {
                return "IM Content";
            }

        };

        FlockNotificationTransport hnt = new FlockNotificationTransport( CHANNEL, ICON_URL,BOT_NAME, TOKEN, APPID, plan, null, null, customVariableContext);

        //dirty reflections trick to inject mock HttpClient
        try
        {
            Field field = FlockNotificationTransport.class.getDeclaredField("client");
            field.setAccessible(true);
            field.set(hnt, new MockHttpClient());
        }
        catch (Exception e)
        {
            e.printStackTrace();
            fail(e.getMessage());
        }

        //hnt.sendNotification(notification);
    }

    public class MockHttpClient extends CloseableHttpClient {

        @Override
        protected CloseableHttpResponse doExecute(HttpHost target, HttpRequest request, HttpContext context) throws IOException, ClientProtocolException {

//            assertTrue(request instanceof HttpPost);
//            HttpPost postMethod = (HttpPost) request;
//            assertEquals(WEBHOOK_URL, request.getRequestLine().getUri());
//
//            try {
//                assert(postMethod.getEntity() instanceof UrlEncodedFormEntity);
//                UrlEncodedFormEntity entity = (UrlEncodedFormEntity) postMethod.getEntity();
//
//                for (NameValuePair vp : URLEncodedUtils.parse(entity))
//                {
//                    if (vp.getName().compareTo("payload")==0)
//                    {
//                        JSONObject payload = new JSONObject(vp.getValue());
//
//                        assertEquals(CHANNEL, payload.getString("channel"));
//                        assertEquals("myBamboo", payload.getString("username"));
//                        assertEquals(ICON_URL, payload.get("icon_url"));
//                    }
//                }
//
//            } catch (JSONException e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//            }

            return null;
        }

        public void close() throws IOException {

        }

        public HttpParams getParams() {
            return null;
        }

        public ClientConnectionManager getConnectionManager() {
            return null;
        }
    }
}
